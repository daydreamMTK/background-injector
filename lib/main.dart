// https://www.raywenderlich.com/6064-image-recognition-with-ml-kit#toc-anchor-014 go here for food detection and ML Kit
// replace all non-food cells with light teal

import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'dart:async';

List<CameraDescription> cameras = await availableCameras();
CameraController controller = CameraController(cameras[0], ResolutionPreset.medium);
String filePath = "Photos/image_test.jpg";
await controller.takePicture(filePath);

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Background Injector',
      theme: ThemeData(primarySwatch: Colors.red),
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
            child: FlatButton(
                onPressed: openCamera(), child: Text('Open Camera'))));
  }
}

openCamera() {}
